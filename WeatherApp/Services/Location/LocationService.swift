//
//  Location.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 27.01.21.
//  Copyright © 2021 Ekaterina Serova. All rights reserved.
//

import Foundation
import CoreLocation

enum Result<T> {
    case success(T)
    case failure(Error)
}

final class LocationService: NSObject {
    static let shared = LocationService()
    private let manager: CLLocationManager
    var newLocation: ((Result<CLLocation>) -> Void)?
    var didChangeStatus: ((Bool) -> Void)?
    var status: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    var isAutorized: Bool {
        status == .authorizedAlways || status == .authorizedWhenInUse
    }
    
    init(manager: CLLocationManager = .init()) {
        self.manager = manager
        super.init()
        manager.delegate = self
    }

    func requestLocationAuthorization() {
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
    }

    func updateLocation() {
        if isAutorized {
            manager.requestLocation()
        }
    }
}

 extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        newLocation?(.failure(error))
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.sorted(by: { $0.timestamp > $1.timestamp }).first {
            newLocation?(.success(location))
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined, .restricted, .denied:
            didChangeStatus?(false)
        default:
            didChangeStatus?(true)
        }
    }
}
