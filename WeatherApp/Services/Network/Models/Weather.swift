//
//  Weather.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 11/25/20.
//  Copyright © 2020 Ekaterina Serova. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let coord: Coord
    let weather: [WeatherInfo]
    let base: String
    let main: Main
    let visibility: Int
    let wind: Wind
    let clouds: Clouds
    let dt: Int
    let sys: Sys
    let timezone: Int
    let id: Int
    let name: String
    let cod: Int
    
    struct Coord: Codable {
        let lon: Double
        let lat: Double
    }

    struct WeatherInfo: Codable {
        let id: Int
        let main: String
        let description: String
        let icon: String
    }
        
    struct Main: Codable {
        let temp: Double
        let feelsLike: Double?
        let tempMin: Double
        let tempMax: Double
        let pressure: Int?
        let humidity: Int?
        let seaLevel: Int?
        let grndLevel: Int?
    }
    
    struct Wind: Codable {
        let speed: Double?
        let deg: Int?
    }
    
    struct Clouds: Codable {
        let all: Int?
    }
    
    struct Sys: Codable {
        let type: Int?
        let id: Int?
        let message: Double?
        let country: String?
        let sunrise: Int?
        let sunset: Int?
    }
}

