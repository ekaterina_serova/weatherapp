//
//  NetworkService.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 11/25/20.
//  Copyright © 2020 Ekaterina Serova. All rights reserved.
//

import Alamofire
import Foundation

struct NetworkService {
    
    static func loadWeather(lat: String, lon: String, successHandler: @escaping ((Weather) -> Void), failureHandler: @escaping ((Error) -> Void)) {
        let url = NetworkService.getCurrentWeatherDataURL(lat, lon)
        let request = AF.request(url, method: .get)
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        request.responseDecodable(of: Weather.self, decoder: decoder, completionHandler: { response in
            if let error = response.error {
                failureHandler(error)
            } else if let weather = response.value {
                successHandler(weather)
            }
        })
    }
    
    private static func getCurrentWeatherDataURL(_ lat: String, _ lon: String) -> String {
        let url = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=894bf6e67fb4054d36007c0230ddb20e&units=metric"
        return url
    }
}

