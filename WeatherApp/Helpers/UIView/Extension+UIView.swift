//
//  Extension+UIView.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 11/3/20.
//  Copyright © 2020 Ekaterina Serova. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T // swiftlint:disable:this force_cast
    }
}
