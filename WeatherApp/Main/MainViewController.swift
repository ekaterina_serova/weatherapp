//
//  ViewController.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 11/2/20.
//  Copyright © 2020 Ekaterina Serova. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation

class MainViewController: UIViewController {
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var weatherLable: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var maxMinTemp: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var stackView: UIStackView!
    
    private var availableTypes: [ViewTypes] = [.hourlyWeather]
    var collectionView: UICollectionView?
    let mainView = UIView()
    var weather: Weather?
    let locationService = LocationService.shared
    
    var isLoading = false {
        didSet {
            updateLayout()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = true
        setupCurrentLocationCoordinates()
        
        self.locationService.didChangeStatus = { [weak self] status in
            guard status else {
                return 
            }
            self?.locationService.updateLocation()
        }
        
        if !locationService.isAutorized {
            locationService.requestLocationAuthorization()
        } else {
            self.locationService.updateLocation()
        }
    }
    
    @IBAction func swipeDown(_ sender: UISwipeGestureRecognizer) {
        print("View was refresh")
        locationService.updateLocation()
    }
    
    private func loadData(locationLat: String, locationLon: String) {
        isLoading = true
        NetworkService.loadWeather(lat: locationLat, lon: locationLon, successHandler: { [weak self] weather in
            self?.weather = weather
            self?.configure(weather)
            self?.isLoading = false
        }, failureHandler: { error in
            print("response error: \(error)")
        })
    }
    
    private func setupCurrentLocationCoordinates() {
        locationService.newLocation = { [weak self] result in
            switch result {
            case .success(let location):
                let locationLat = "\(location.coordinate.latitude)"
                let locationLon = "\(location.coordinate.longitude)"
                
                self?.loadData(locationLat: locationLat, locationLon: locationLon)
            case .failure(let error):
                assertionFailure("Error getting the users location \(error)")
            }
        }
    }
    
    private func configure(_ mainWeather: Weather) {
        let name = mainWeather.name
        let description = mainWeather.weather.first?.description.capitalized
        let temp = mainWeather.main.temp
        let maxTemp = mainWeather.main.tempMax
        let minTemp = mainWeather.main.tempMin
        
        placeLabel.text = name
        descriptionLabel.text = description
        tempLabel.text = String(Int(temp))
        maxMinTemp.text = getMaxMinTempCurrentData(maxTemp, minTemp)
    }
    
    private func getMaxMinTempCurrentData(_ maxTemp: Double, _ minTemp: Double) -> String {
        let maxMinTempCurrentData = "max. \(maxTemp)° | min. \(minTemp)°"
        return maxMinTempCurrentData
    }
    
    private func updateLayout() {
        placeLabel.isHidden = isLoading
        weatherLable.isHidden = isLoading
        descriptionLabel.isHidden = isLoading
        tempLabel.isHidden = isLoading
        maxMinTemp.isHidden = isLoading
        
        if isLoading {
            if !spinner.isAnimating {
                spinner.startAnimating()
                spinner.isHidden = false
            }
        } else if spinner.isAnimating {
            spinner.stopAnimating()
        }
    }
}
