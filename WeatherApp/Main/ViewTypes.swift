//
//  ViewTypes.swift
//  WeatherApp
//
//  Created by Siarova Katsiaryna on 11/3/20.
//  Copyright © 2020 Ekaterina Serova. All rights reserved.
//

import Foundation

enum ViewTypes {
    case daysWeather
    case hourlyWeather
}
